package readability;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

/**
 * Word counter which count the word, syllables, and sentences.
 * Then compute the readability of the text.
 * @author Prin Angkunanuwat
 *
 */
public class WordCounter {
	private final State START = new START( );
	private final State CONSONANT = new CONSONANT( );
	private final State E_FIRST = new E_FIRST( );
	private final State VOWEL = new VOWEL( );
	private final State DASH = new DASH( );
	private final State NONWORD = new NONWORD( );
	private final State ENDSENTENCE = new ENDSENTENCE( );
	private State state = START;

	private int wordCount;
	private int syllablesCount;
	private int syllables;
	private int sentencesCount;
	private double index;
	/**
	 * Count the syllables of the word.
	 * @param word is a string we need to count its syllables.
	 * @return count of syllables.
	 */
	public int countSyllables(String word) {
		setState(START);
		for( char c : word.toCharArray() ) { 
			if( state == NONWORD ) return 0;
			state.handleChar(c);
			
		}
		if( state == DASH) return 0;
		if( state == E_FIRST && syllables ==0 ) syllables++;
		if( state == ENDSENTENCE ) {
			System.out.println(word);
			sentencesCount++;
		}
		if( syllables != 0) wordCount++;
		return syllables;

	}
	/**
	 * Check if it's vowel or not.
	 * @param c is character we want to check.
	 * @return true if c is vowel;
	 */
	private boolean isVowel(char c) {
		return "AEIOUaeiou".indexOf(c) >= 0; 

	}
	/**
	 * Check if c is letter or not.
	 * @param c is character we want to check.
	 * @return true if c is letter.
	 */
	public boolean isLetter(char c) {
		return Character.isLetter(c);
	}
	/**
	 * Check if c is space, tab, new line.
	 * @param c is character we want to check.
	 * @return true if c is space,tab, new line.
	 */
	public boolean isWhitespace(char c) {
		return Character.isWhitespace(c);
	}
	
	public boolean isEndSentence(char c) {
		return ".;?!".indexOf(c) >= 0;
	}
	
	/**
	 * Set state.
	 * @param state we need to set.
	 */
	public void setState( State newstate ) {
		if (newstate != state) newstate.enterState();;
		state = newstate;
	}
	/**
	 * Get readability compute from index.
	 * @return readability of the text.
	 */
	public String getReadability(){
		String readability = "";
		if( index < 0 ) readability = "Advanced degree graduate";
		else if( index <= 30 ) readability = "College graduate";
		else if( index <= 50 ) readability = "College student";
		else if( index <= 60 ) readability = "High school student";
		else if( index <= 65 ) readability = "9th grade student";
		else if( index <= 70 ) readability = "8th grade student";
		else if( index <= 80 ) readability = "7th grade student";
		else if( index <= 90 ) readability = "6th grade student";
		else if( index <= 100 ) readability = "5th grade student";
		else readability = "4th grade student";
		return readability;
	}
	/**
	 * Count the word from instream.
	 * @param instream we need to read.
	 * @return count of the word.
	 */
	public int countWords(InputStream instream) {
		BufferedReader breader = new BufferedReader( new InputStreamReader(instream) );
		String line;
		sentencesCount = 0;
		syllablesCount = 0;
		wordCount = 0;
		try {
			while(true){

				line = breader.readLine();
				if ( line == null ) break;
				StringTokenizer token =  new StringTokenizer(line,",  : ( ) [ ] / \" ");
				while(token.hasMoreTokens()){
					syllablesCount += countSyllables(token.nextToken());
				}
			}
		} catch (IOException e1) {
			return -1;
		}
		index = 206.835 - 84.6 * ( syllablesCount / wordCount ) - 1.015 * ( wordCount / sentencesCount );
		return wordCount;
	}
	/**
	 * Count the word from url.
	 * @param url that contain text.
	 * @return Count of the word in url.
	 * 
	 */
	public int countWords(URL url) {
		InputStream in = null;
		try {
			in = url.openStream( );
		} catch (IOException e) {
			return -1;
		}
		return countWords(in);
	}
	/**
	 * Get syllables from recent countWords.
	 * @return syllables from recent countWords.
	 */
	public int getSyllableCount() {
		return syllablesCount;
	}
	/**
	 * Get sentences from recent countWords.
	 * @return sentences from recent countWords.
	 */
	public int getSentenceCount() {
		return sentencesCount;
	}
	/**
	 * Get index from recent countWords.
	 * @return index from recent countWords.
	 */
	public double getIndex(){
		return index;
	}
	/**
	 * State interface has handleChar and enterState method.
	 */
	public interface State {
		/**
		 * Handle the char and choose which state will be set.
		 * @param c is char we will handle.
		 */
		public void handleChar(char c);
		/**
		 * Perform action when enter state.
		 */
		public void enterState( );
	}

	class START implements State {

		@Override
		public void handleChar(char c) {
			if ( isWhitespace(c) ) ;
			
			else if ( !isLetter(c) ) setState( NONWORD );
			else if  (c=='e' || c=='E' ) setState( E_FIRST );
			else if ( isVowel(c) || c=='y' || c=='Y' ) setState( VOWEL );
			else if ( isLetter(c) ) setState( CONSONANT );
		}

		@Override
		public void enterState() { syllables = 0; }

	}

	class VOWEL implements State {
		@Override
		public void handleChar( char c ) {
			if ( c=='\'' || isVowel(c) ) ;
			else if ( c=='-' ) setState( DASH ); 
			else if ( isEndSentence(c) ) setState( ENDSENTENCE ) ;
			else if ( !isLetter(c) ) setState( NONWORD );
			else if ( isLetter(c) ) setState( CONSONANT ); 
		}
		@Override
		public void enterState( ) { syllables++; }
	}

	class CONSONANT implements State {

		@Override
		public void handleChar(char c) {
			if ( c=='\'' ) ;
			else if ( c=='-' ) setState( DASH );
			else if ( isEndSentence(c) ) setState( ENDSENTENCE ) ;
			else if ( !isLetter(c) ) setState( NONWORD );
			else if ( c=='e' || c=='E' ) setState( E_FIRST );
			else if ( isVowel(c) || c=='y' || c=='Y' ) setState( VOWEL );
		}

		@Override
		public void enterState() {}
	}

	class DASH implements State {

		@Override
		public void handleChar(char c) {
			if ( !isLetter(c) ) setState( NONWORD );
			else if ( c=='e' || c=='E' ) setState( E_FIRST );
			else if ( isVowel(c) || c=='y' || c=='Y' ) setState( VOWEL );
			else if ( isLetter(c) ) setState( CONSONANT );
		}

		@Override
		public void enterState() {}
	}

	class E_FIRST implements State {

		@Override
		public void handleChar(char c) {
			if ( c=='\'' ) ; 
			else if ( c=='-' ) {
				syllables++;
				setState( DASH );
			}
			else if ( isEndSentence(c) ) {
				syllables++;
				setState( ENDSENTENCE ) ;
			}
			else if ( !isLetter(c) ) setState( NONWORD );
			else if ( isVowel(c) ) setState( VOWEL );
			else if ( isLetter(c) ) {
				syllables++;
				setState( CONSONANT );
			}
		}

		@Override
		public void enterState() {}
	}

	class NONWORD implements State {

		@Override
		public void handleChar(char c) {}

		@Override
		public void enterState() { syllables = 0; }
	}
	
	class ENDSENTENCE implements State {

		@Override
		public void handleChar(char c) {
			if( !isWhitespace(c) ) setState( NONWORD );
		}

		@Override
		public void enterState() {}
		
	}

}


