package readability;
/**
 * Main class to run the program.
 * @author Prin Angkunanuwat
 *
 */
public class Readability {
	/**
	 * Main method to run the program.
	 * @param args is not used.
	 */
	public static void main(String[] args){
		GraphicalUI demo = new GraphicalUI();
	}
}
