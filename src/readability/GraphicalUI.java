package readability;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.StringTokenizer;

/**
 * Gui class for readability.
 * @author Prin Angkunanuwat
 *
 */
public class GraphicalUI extends JFrame {
	private JLabel label;
	private JTextField input;
	private JButton browse;
	private JButton count;
	private JButton clear;
	private JTextArea output;
	private JScrollPane scroller;

	private String fileName = "C:/";
	private final WordCounter counter = new WordCounter();
	/**
	 * Constructor, set some property.
	 */
	public GraphicalUI(){
		super("Readability by Prin Angkunanuwat");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(500, 400);
		this.setResizable(false);
		this.initComponents();
		this.setVisible(true);

	}
	/**
	 * Init components of this gui.
	 */
	private void initComponents(){
		Container contents = this.getContentPane();
		LayoutManager layout = new FlowLayout( );
		contents.setLayout( layout );

		label =  new JLabel("File or URL name: ");
		input = new JTextField(20);
		browse = new JButton("Browse...");
		count = new JButton("Count");
		clear = new JButton("Clear");
		output = new JTextArea(17,40);
		scroller = new JScrollPane(output);

		contents.add( label );
		contents.add( input );
		contents.add( browse );
		contents.add( count );
		contents.add( clear );
		contents.add( scroller );

		browse.addActionListener(new BrowseListener());
		clear.addActionListener(new ClearListener());
		count.addActionListener(new CountListener());

	}
	/**
	 * ActionListener for brown button.
	 * Search the file in the directory.
	 */
	class BrowseListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			fileName = getFilename();
			input.setText(fileName);
		}
	}
	/**
	 * ActionListener for clear button.
	 * Clear all text to be empty.
	 */
	class ClearListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			input.setText("");
			output.setText("");
		}
	}
	/**
	 * ActionListener for count button.
	 * Get the wordscount, syllablescount, sentencescount, index, and readability.
	 * Report it in the textArea.
	 */
	class CountListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed( ActionEvent evt ) {
			int words = 0;
			boolean success = false;

			String text = input.getText();
			if( text.indexOf("http://") >= 0 || text.indexOf("https://") >= 0 
					|| text.indexOf("ftp://") >= 0 || text.indexOf("file:/") >= 0) {
				URL url = null;
				try {
					url = new URL( text );
					words = counter.countWords( url );
					success = true;
				} catch (MalformedURLException e) {
					output.setText("Unable to process!");
				}
			} else {
				try {
					InputStream ins = new FileInputStream(text);
					words = counter.countWords(ins);
					success = true;
				} catch (FileNotFoundException e) {
					output.setText("Unable to process!");
				}
			}
			if( success ){
				StringTokenizer token = new StringTokenizer(text,"/"); 
				String name = "";
				while(token.hasMoreTokens()){
					name = token.nextToken();
				};
				String out = String.format("%s \n\n%-50s%s "
						+ "\n%-50s%d \n%-50s%d \n%-50s%d \n%-50s%f \n%-50s%s",
						text,"Filename:",name,"Number of Syllables:",counter.getSyllableCount(),
						"Number of Words:",words,"Number of Sentences:",counter.getSentenceCount(),
						"Flesch Index:",counter.getIndex(),"Readability:",counter.getReadability());
				output.setText(out);
			}
		}
	}

	/**
	 * Get the file name from directory.
	 * @return name of the file that is choose.
	 */
	public String getFilename() {
		// FileChooser object display a File dialog.
		JFileChooser fc = new JFileChooser();
		// Define a file filter so that only compatible files are shown.
		// In this case, we want only text files.
		FileFilter filter = new TextFileFilter();
		// add the file filter
		fc.addChoosableFileFilter( filter );
		// hack, hack -- make C:/ the starting directory
		// You should change this to remember the last directory
		// that the user opened.
		if( fileName != null) fc.setCurrentDirectory( new File(fileName) );
		else fc.setCurrentDirectory( new File("C:/") );

		int result = fc.showDialog(null, "Open");
		// If user selected a file then fc.getSelectedFile() is a File 
		// object containing that file.  Get the filename of that file.
		// If you prefer, you can just return the File object itself.
		if ( result == JFileChooser.APPROVE_OPTION ) 

			return fc.getSelectedFile().getAbsolutePath();
		else return null;
	}
	/**
	 * Define a file filter for text files.  
	 * javax.swing.filechooser.FileFilter is an abstract class; 
	 * you create a subclass that implements the 
	 * accept() and getDescription() methods.
	 * 
	 * In this example, we return true for files with extension .txt
	 *
	 * Caveat: this does *NOT* use the java.io.FileFilter interface.
	 */
	class TextFileFilter extends FileFilter {
		/** accept(File f) method required by the interface */
		public boolean accept(File f) {
			if ( f.isDirectory() ) return true;
			if ( ! f.isFile() ) return false;
			String ext = getExtension(f);
			if ( ext.equals("txt") ) return true;
			/* to include HTML files also add these extensions
			if ( ext.equals("htm") ) return true;
			if ( ext.equals("html") ) return true;
			 */
			// anything else return false
			return false;
		}

		/** Describe this file filter.  Will be displayed in dialog. */
		public String getDescription() {
			return "Text (.txt) Files";
		}
		/** utility method used by accept()
		 *  it returns file extension of the parameter's filename
		 */
		String getExtension(File f) {
			String filename = f.getName();
			int k = filename.lastIndexOf('.');
			if ( k <= 0 || k >= filename.length() -1 ) return "";
			else return filename.substring(k+1).toLowerCase();
		}
	} // end of TextFileFilter class
}
